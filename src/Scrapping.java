import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.net.*;
import java.io.*;
import java.util.*;

import static java.lang.System.exit;

public class Scrapping {

    LinkedList<String> dataList;

    public Scrapping() {
        dataList = new LinkedList<>();
    }

    public void scraped(String url) {
        String html = getUrl("https://en.wikipedia.org/" + url);
        Document doc = Jsoup.parse(html);
        String contentText = doc.select("ul").first().text();
        addText(contentText);
        writetoFile(contentText);
    }

    private String getUrl(String url){
        URL urlLinked = null;
        try{
            urlLinked = new URL(url);
        } catch(MalformedURLException e){
            System.out.println("The url was not found!");
            return "";
        }
        URLConnection connect = null;
        BufferedReader read = null;
        String result = "";
        try{
            connect = urlLinked.openConnection();
            read = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            String nextline = "";
            while((nextline = read.readLine()) != null){
                result += nextline;
            }
            read.close();
        } catch(IOException e){
            System.out.println("There was an error connecting to the URL");
            return "";
        }
        return result;
    }

    private void addText(String data) {
        String[] splitData = data.split("\\s+");

        for (int i= 0; i < splitData.length ; i++) {
            dataList.add(splitData[i]);
        }
    }

    private static void writetoFile(String data) {
        try{
            FileWriter fw=new FileWriter("src/scrappedData.txt" , true);
            fw.write(data);
            fw.write("\n");
            fw.write("===============================================================================================================================================================\n\n");
            fw.close();
        }catch(Exception e){System.out.println(e);}
        System.out.println("Data Save Successfully...");
    }

    public void printList() {
        for(String data: dataList) {
            System.out.println(data);
        }
    }

    public void readData() {
        try (BufferedReader br = new BufferedReader(new FileReader("src/scrappedData.txt"))) {
            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        Scrapping srp = new Scrapping();
        Scanner scanner = new Scanner(System.in);
        int again = 0;
        do {
            System.out.println("Enter the Choise : ");
            System.out.println("1) Search For Data Online. ");
            System.out.println("2) Show Existing Data. ");
            System.out.println("3) Exit.");
            int cho = scanner.nextInt();

            if(cho == 1) {
                System.out.println("Enter the Search: ");
                String search = scanner.next();
                String searched = "wiki/" + search;
                srp.scraped(searched);
                srp.printList();
            } else if (cho == 2) {
                System.out.println("Print the data (yes = y  and  no = n): ");
                char choise = scanner.next().charAt(0);
                if(choise == 'Y' || choise == 'y') {
                    srp.printList();
                } else {
                    System.out.println("finished");
                }
            } else if (cho == 3) {
                exit(0);
            } else {
                System.out.println("\nInvalid Choise.\n");
            }


            System.out.println("\n\nPress 4 to Search again : ");
            again = scanner.nextInt();

            System.out.println("\n\n================================================================================\n\n");

        }while(again == 4);
    }

}
